syntax = "proto3";
package com.skllzz.api;
import "common/annotations.proto";
import "common/common.proto";
import "common/stat.proto";
option go_package = "gitlab.com/skllzz/back/services/pkg/api/skllzz/challenge;skllzz_challenge";
option java_multiple_files = false;
option java_package = "com.skllzz.api";
option java_outer_classname = "ChallengeType";
option objc_class_prefix = "ChallengeType";

enum Activity {
  STEPS = 0; // учитывать активность из шагов
  WORKOUTS = 1;  // учитывать активность из тренировок
}

// Правила соревнования
message Rules {
  enum WinnerAwardMethod {
    NONE = 0; // без вознаграждения победителей
    SINGLE_WINNER = 1; // все главному победителю
    PROPORTIONALLY_FOR_TOP = 2; // пропорционально 50% 30% 20%
  }
  enum RankGoal {
    SKLLZZ = 0; // позицию в ранке определяют скилзы
    METERS = 1; // позицию в ранке определяют метры
  }
  // Источники активности для соревнования
  string name = 2 [(searchable) = true]; // название соревнования
  int64  begin_seconds = 3 [deprecated = true]; // epoch секунды начала соревнования (0 если сразу с момента создания)
  int64 first_activity_day = 100; // первый день активности которая пойдет в зачет
  int64  end_seconds = 4 [deprecated = true]; // epoch секунды окончания соревнования (0 если без ограничений)
  int64 last_activity_day = 101; // последний день активности которая пойдет в зачет
  repeated Activity source = 5; // источники активности которые идут в зачет
  repeated TrainingSession.Kind workout_types = 6; // типа тренировок которые идут в зачет если в источниках есть WORKOUTS (если пустой то учитывать все)
  RankGoal rank_goal = 7; // относительно чего составлять ранк (скилзы или метры)
  // правила соревнования
  oneof rules {
    AbsoluteLeaderRule absolute_leader = 10 [(searchable) = true];
    HistoricalLeaderRule historical_leader = 11 [(searchable) = true];
    SharedGoalRule shared_goal = 12 [(searchable) = true];
    GroupRules group_rules = 13;// правила для групповых соревнований
  }
  int32 max_participants = 21; // максимальное число участников (0 если без ограничения)
  bool with_chat = 22 [deprecated = true]; // создать общий чат для соревнования в телеграм
  string description = 30 [(searchable) = true]; // более детальное описание соревнования
  double paid = 40; // платное соревнование требующее оплаты за участие
  WinnerAwardMethod award_method = 41; // способ поделить куш
  bool with_artifacts = 50; // разрешить действие артефактов на результаты тренировок
  string photo_url = 60; // картинка к соревнованию
  string logo_url = 61; // логотип к соревнованию
  string chat_url = 80; // связанный чат
  string require_club = 81; // требовать обязательное подключение к клубу с заданным кодом для участия в соревновании (если пусто то без ограничений)
}

//  правила для групповых соревнований
message GroupRules {
  enum RankMode {
    AVERAGE = 0;
    TOTAL = 1;
  }
  int32 max_teams = 1; // максимальное число команд
  RankMode mode = 2; // режим командного учета
  // правила соревнования команды
  oneof team_rules {
    AbsoluteLeaderRule absolute_leader = 10 [(searchable) = true];
  }
}

// Режим соревнования с абсолютным лидером
message AbsoluteLeaderRule {
  // набранное участником значение которые завершат соревнования досрочно при достижении его любым участником (0 если без такого ограничения)
  double stop_value = 1;
}

// Режим соревнования с лидером набравшим максимальное количество sz за указанное число последних календарных дней
message HistoricalLeaderRule {
  // количество дней за которые учитывать skllzz
  int32 days = 1;
}

// Режим соревнования с общей целью
message SharedGoalRule {
  // массив целей, от промежуточных до финальной
  repeated GoalStep goal_step = 1;
}

message GoalStep {
  double value = 1; // значение промежуточной цели в резмерности определяемой RankGoal
  string description = 2;
}

//Идентификатор участника в соревновании
message LeaderboardMember {
  // идентификатор лидерборда
  string leaderboard_id = 1 ;
  // идентификатор участника соревнования (совпадает с profile.id)
  string member_id = 2;
  // возвращать данные с отметкой синхронизации превышающую заданную
  int64  sync_millis = 3;
}

//Идентификатор активности участника в соревновании
message LeaderboardMemberActivity {
  // идентификатор лидерборда
  string leaderboard_id = 1 ;
  // идентификатор участника соревнования (совпадает с profile.id)
  string member_id = 2;
  // идентификатор тренировки
  string session_id = 3;
  // отметка времени для синхронизации
  int64 sync_millis = 4;
  // Возвращать данные с детализацией
  bool with_details = 5;

}

//Жалоба на участника соревнования
message LeaderboardMemberClaim {
  // идентификатор лидерборда
  string leaderboard_id = 1 ;
  // идентификатор участника соревнования (совпадает с profile.id)
  string member_id = 2;
  // Причина жалобы
  string reason = 3;
  // время создания жалобы
  int64 created_millis = 4;
  // время последнего изменения
  int64 last_modified_millis = 5;
}


message Member {
  message Claims {
    // общее число жалоб
    int32 total = 1;
    // процентное число жалоб относительно всех участников
    float percent = 2;
  }
  // идентификатор участника соревнования (совпадает с profile.id)
  string id = 1 [(searchable) = true];
  // никнейм участника
  string nick_name = 2 [(searchable) = true];
  // аватар участника
  string avatar_url = 3;
  // заработано в соревновании (резмерность определяется правилами соревнования в RankGoal)
  double  earned_value = 4;
  // позиция в соревновании
  int32 rank = 5 [(transient) = true];
  // время присоединения к соревнованию
  int64 join_millis = 6;
  // время последнего изменения
  int64 last_modified_millis = 7;
  // признак дисквалификации
  bool disqualified = 8;
  // Игровой возраст
  int32 game_age_days = 9;
  // признак наличия подписки на pro
  bool pro = 10;
  // предпочитаемый языкучастника
  string lang = 12;
  // статистика по жалобам
  Claims claims = 13;
  // признак удаления
  bool  deleted = 999;
}

// Данные об активности участника в лидерборде
message LogRecord {
  // уникальный идентификатор активности
  string id = 1;

  // Заработанные или потраченные skllzz (положительная - заработанные, отрицательная - потраченные)
  double skllzz = 2;

  // Описание активности
  string description = 3;

  // время начала действия (epoch seconds)
  int64 stamp_seconds = 4;
  // длительность действия (может быть нулевым)
  int64 duration_seconds = 5;

  // номер дня активности в часовом поясе клиента
  int64 activity_day = 6;

  // Тип активности
  oneof details  {
    TrainingSession training = 10;
  }
  // признак удаления
  bool  deleted = 999;
}

enum Role {
  // участник
  MEMBER = 0;
  // наблюдатель
  VIEWER = 1;
  // предложение
  OFFER = 2;
}

message LeaderboardDesign {
  //идентификатор лидерборда
  string id = 1 ;
  // название соревнования (если пусто то не менять)
  string name = 2;
  // более детальное описание соревнования (если пусто то не менять)
  string description = 30;
  // картинка к соревнованию (если пусто то не менять, для удаления передать строку "-")
  string photo_url = 60;
  // логотип к соревнованию (если пусто то не менять, для удаления передать строку "-")
  string logo_url = 61;
  // новый id хозяина лидерборда (если пусто то не менять)
  string new_author_id = 70;
  // новый адрес чата
  string chat_url = 80;
}

// Отношение профиля к лидерборду
message LeaderboardRole {
  //идентификатор лидерборда
  string id = 1 ;
  // время создания связи
  int64 created_millis = 2;
  // Роль по отношению к лидерборду
  Role role = 3;
  // время последнего изменения отношения
  int64 last_modified = 4;
  int32 version = 5; // версия роли (0,2)
  // признак удаления
  bool  deleted = 999;
}

// Лидерборд (игра)
message Leaderboard {
  // Состояние соревнования
  enum State {
    // соревнование ожидает начала
    IDLE = 0;
    // соревнование активно
    ACTIVE = 1;
    // соревнование завершено
    COMPLETED = 2;
    // соревнование удалено
    DELETED = 999;
  }
  // правила соревнования
  Rules rules = 2 [(searchable) = true];
  // ссылка для приглашения участника
  string invite = 3;
  // ссылка для приглашения наблюдателя
  string invite_viewer = 4;
  // время создания соревнования
  int64 created_millis = 5;
  // ссылка на общий чат или пусто если чат не создан (устарело, используем ссылку из Rules)
  string chat_url = 6 [deprecated = true];
  // идентификатор профиля автора/президента/капитана (если пустой то соревнование создано не клиентом)
  string author_id = 20 [(searchable) = true];
  // состояние лидерборда
  State state = 21 [(searchable) = true];
  // временная зона в которой создано соревнование
  string timezone = 23 [(searchable) = true];
  // время проведения следующих выборов президента лидерборда
  int64 next_election_millis = 25;
  double jackpot = 26 [(transient) = true]; // призовой куш
  map<int64, double> prizes = 27 [(transient) = true]; // сумма призов по местам
  int32 version = 30; // версия лидерборда (0,2)
}

message LeaderboardRank {
  //идентификатор лидерборда
  string id = 1 ;
  // время последнего изменения в рейтинге
  int64 last_modified = 2;
  // топ участников в порядке лидерства (обязательно включает текущего пользователя)
  repeated Member members = 3;
  // лидерборд
  Leaderboard leaderboard = 4;
  // суммарные достижения всех участников лидерборда
  double earned_total = 5;
  // общее количество всех участников
  double member_total = 6;
  // нормализованная величина эффективности участников лидерборда (от 0 до 1)
  double normalized_performance = 7;
  // время epoch seconds, когда будут подведены окончательные итоги
  int64 final_result_stamp = 8;
  // время epoch seconds, когда будет запущено соревнование
  int64 activation_stamp = 9;
  // статистика соревнования
  PeriodStat stats = 10;
}



// данные по пульсу или шагам конкретной тренировки
message ActivityDetails {
  repeated StepsData steps = 1;
  repeated TrainingData hr = 2;
}

message Challenge {
  string id = 1; // идентификатор соревнования
  string parent_id = 2; // идентификатор родительского соревнования если часть командного
  string club_id = 3; // принадлежность к клубу
  Leaderboard leaderboard = 4; // правила и метаданные соревнования
  bool public = 5; // показывать на витрине клуба
  bool private = 6; // приватный лидерборд (не предлагать присоединиться к нему, а только показывать ранк из панели предложений)

}

message ChallengeSyncRequest {
  enum Type {
    unknown = 0;
    challenge_info = 1; // информация о соревновании Challenge
    child_challenge_info = 2; // информация о подчиненном соревновании Challenge
    challenge_rank = 3; // информация о турнирной таблице соревнования LeaderboardRank
  }
  int64 sync_seq_no = 2; // последний известный порядковый элемента
  string challenge_id = 3; // идентификатор соревнования
}

message ChallengeMemberSyncRequest {
  enum Type {
    unknown = 0;
    training_info = 1; // информация об активности внутри соревнования TrainingSession
    member_info = 2; // информация об участнике соревнования Profile (не для групповых)
  }
  int64 sync_seq_no = 2; // последний известный порядковый элемента
  string challenge_id = 3; // идентификатор соревнования
  string member_id = 4; // идентификатор участника
}

// запись о дисквалификации
message DisqualifyRecord{
  int64 stamp = 1; // epoch seconds время дисквалификации
  string reason = 2; // причина дисквалификации
}
