syntax = "proto3";
package com.skllzz.api;
import "common/common.proto";
import "club/trainers.proto";
import "club/rooms.proto";
import "google/protobuf/timestamp.proto";
option go_package = "gitlab.com/skllzz/back/services/pkg/api/skllzz/club;skllzz_club";
option java_multiple_files = false;
option java_package = "com.skllzz.api";
option java_outer_classname = "EnrolmentType";
option objc_class_prefix = "EnrolmentType";

// интеграционный сервис для получения информации о расписании клуба
service ClubSchedule {
  // запрос данных о расписании
  rpc List(ScheduleRange) returns (Appointments);
}
// интеграционный сервис для получения информации о тренерах клуба
service TrainersInfo {
  // запрос данных о тренерском составе
  rpc List(TrainersRange) returns (Trainers);
}

message TrainersRange {
  // обязательный ключ для авторизации отправителя (единый на клуб)
  string key = 1;
}
message Trainers {
  repeated TrainerInfo list = 1;
}

message Appointments {
  repeated Appointment list = 1;
}

message ScheduleRange {
  // обязательный ключ для авторизации отправителя (единый на клуб)
  string key = 1;
  // начальный интервал
  google.protobuf.Timestamp from = 2;
  // конечный интервал
  google.protobuf.Timestamp till = 3;
}

// направление или группа занятий
message Group {
  string id = 1; // идентификатор группы
  string title = 2; // название группы
  string logo_url = 3; // лого для группы (не обязательно)
  string header_logo_url = 4; // лого для группы (не обязательно)
  repeated string tags = 5; // список кастомных меток у предложений для этой группы
}

// отдельно взятая услуга
message Service {
  // уровень сложности
  enum Difficulty {
    unknown = 0; // не определен
    easy = 1; // простой
    medium = 2; // средний
    hard = 3; // сложный
  }
  string id = 1; // идентификатор услуги
  string title = 2; // краткое название услуги
  string description = 3; // описание услуги
  Group course = 4; // группа услуг
  map<string, string> tags = 5; // кастомные метки
  IntRange kkal = 6; // расход калорий
  IntRange for_age = 7; // ограничение по возрасту
  Difficulty difficulty = 8; // уровень сложности
}

// отдельно взятое занятие
message Appointment {
  enum ReserveAction {
    none = 0; // никуда не записаны, никакие действия не доступны
    can_cancel_assign = 1; // записаны на занятие, можно выписаться
    can_assign = 2; // есть свободные места, может записаться
    can_add_wait = 3; // нет мест, может добавиться в лист ожидания
    can_cancel_wait = 4; // находится в листе ожидания и может из него удалиться
    can_confirm_wait = 5; // подошла очередь в листе ожидания и можно подтвердить или отменить запись
    no_free_slots = 6; // мест нет, никаких других действий не доступно
  }
  enum Status {
    active = 0;
    canceled = 1;
  }
  enum Class {
    in_group = 0;
    personal = 1;
  }
  string id = 1; // идентификатор занятия
  Service service = 2; // предоставляемая услуга
  Status status = 3; // статус занятия
  int64 stamp_seconds = 4; // время начала занятия в epoch seconds
  int64 duration_seconds = 5; // продолжительность занятия в секундах
  //int64 sync_seq_no = 6; // порядковый номер изменения
  Group group = 7; // группа
  Room room = 8; // помещение
  TrainerInfo trainer = 9; // данные по тренеру
  Class class = 10; // тип занятия (персональное, групповое)
  bool pre_reserve = 11 [deprecated = true]; // требует предварительной записи (используется reserve_action)
  float cost = 12; // занятие за доп плату (0 если бесплатное)
  int32 capacity = 13; // максимальное количество участников (0 не ограничено)
  int32 free_slots = 14; // количество свободных мест в случае предварительной регистрации (-1 если не известно или не ограничено)
  string reserve_id = 15; // идентификатор записи на занятие (пустой если клиент не записан)
  bool commercial = 16; // признак платного урока
  string online_class_url = 17; // url для трансляции занятия
  int32 epoch_day = 18; // epoch day занятия
  int32 epoch_day_day_seconds = 19; // старт занятия в секундах относительно начала дня
  OnlineClass.Category online_class_category = 20; // категория онлайн занятия
  ReserveAction reserve_action = 21; // возможные действия по брони
  Int64Range booking_window_seconds = 22; // время в epoch seconds начала и окончания возможности записи на занятие (если 0 то не задано)
  string wait_list_message = 23; // сообщение клиенту в связи с ожиданием, например: Вы добавлены в лист ожидания. Ваш номер в очереди: 1
  bool deleted = 999; // признак удаления
  int32 version = 1000; // версия структуры
}

//список всех мероприятий на день
message AppointmentsDay {
  repeated Appointment list = 1;
  int64 day = 2; // порядковый номер дня
}

message OnlineClass {
  //SHORT - короткие занятия до 20 минут
  //MEDIUM - обычные занятия менее 40 минут
  //LONG - долгие занятия более 40 минут
  enum Category {
    UNKNOWN = 0;
    CARDIO_SHORT = 1020;
    CARDIO_MEDIUM = 1040;
    CARDIO_LONG = 1060;
    STRETCH_SHORT = 2020;
    STRETCH_MEDIUM = 2040;
    STRETCH_LONG = 2060;
    POWER_SHORT = 3020;
    POWER_MEDIUM = 3040;
    POWER_LONG = 3060;
    DANCE_SHORT = 4020;
    DANCE_MEDIUM = 4040;
    DANCE_LONG = 4060;
  }
  string start = 1;
  Category category = 2;
}

message WeekDaysRestriction {
  enum Day {
    Sunday = 0;
    Monday = 1;
    Tuesday = 2;
    Wednesday = 3;
    Thursday = 4;
    Friday = 5;
    Saturday = 6;
  }
  repeated Day day = 1;
}
message ScheduleRestrictions {
  oneof type {
    WeekDaysRestriction  week_days = 1;
  }
}

message OnlineStrategy {
  repeated ScheduleRestrictions restrictions = 2;
  repeated OnlineClass classes = 1;
}