syntax = "proto3";
package com.skllzz.api;
option go_package = "gitlab.com/skllzz/back/services/pkg/api/skllzz/common;skllzz_common";
option java_multiple_files = false;
option java_package = "com.skllzz.api";
option java_outer_classname = "ErrorType";
option objc_class_prefix = "ErrorType";

message SkllzzError {
  enum Code {
    // нет специального кода ошибки
    UNKNOWN = 0;
    // запрошен отсуствующий лидерборд
    DELETED_LEADERBOARD = 1;
    // превышено число лидерборда для пользователя
    LEADERBOARD_LIMIT_EXCEEDED = 2;
    // превышено число участников лидерборда
    LEADERBOARD_MEMBER_LIMIT_EXCEEDED = 3;
    // недопустимая настройка числа участников
    LEADERBOARD_INVALID_PARTICIPANTS_CONFIG = 4;
    // недопустимая настройка длительности соревнования (не менее 3 и не более 30 дней)
    LEADERBOARD_INVALID_DURATION = 5;
    // недопустимая настройка стоп числа SKLLZZ в соревновании (не менее 300 и не более 30000 sz)
    LEADERBOARD_INVALID_SKLLZZ_STOP = 6;
    // недопустимая настройка выявления лидера в ретроспективе дней
    LEADERBOARD_INVALID_HISTORICAL_DAYS = 7;
    // Невозможно подключиться к завершенному соревнованию
    LEADERBOARD_UNABLE_TO_JOIN_COMPLETED = 8;
    // Невозможно поменять аватарку по причине недостаточного омоложения
    PROFILE_UNABLE_TO_CHANGE_AVATAR = 9;
    // Уже участник соревнования
    LEADERBOARD_ALREADY_JOINED = 10;
    // Товар не доступен (позможно по причине требования pro статуса, см флаг offer_pro)
    MARKET_PRODUCT_UNAVAILABLE = 11;
    // Товар закончился
    MARKET_PRODUCT_EXHAUSTED = 12;
    // Не достаточно sz для покупки товара
    MARKET_INSUFFICIENT_SKLLZZ = 13;
    // Не достаточно sz для бустера
    IAMOK_INSUFFICIENT_SKLLZZ_TO_BOOST = 14;
    // Уже есть активное ускорение
    IAMOK_ALREADY_BOOSTED = 15;
    // Уже есть активное замедление
    IAMOK_ALREADY_FREEZED = 16;
    // Слишком рано для перемещения назад
    IAMOK_TOO_EARLY_FOR_TELEPORT_BACK = 17;
    // Не достатоно накопленной энергии для перемещения вперед
    IAMOK_INSUFFICIENT_ENERGY_TO_TELEPORT_FORWARD = 18;
    // Нет необходимого артефакта на полке
    GAME_ARTIFACT_SHELF_IS_EMPTY = 19;
    // Артефакт еще не готов (слишком мало прошло времени с последней покупки)
    GAME_ARTIFACT_IS_NOT_READY = 20;
    // Артефакт еще активен и применение нового не требуется
    GAME_ARTIFACT_IS_ALREADY_ACTIVE = 21;
    // Артефакты заморожены вирусом
    GAME_ARTIFACT_IS_FREEZED_BY_VIRUS = 22;
    // Артефакт не совместим с текущим набором
    GAME_ARTIFACT_INCOMPATIBLE = 26;
    // изменения может сделать только автор соревнования
    LEADERBOARD_NOT_AN_AUTHOR = 23;
    // указанный id автора не может стать приемником (диквалифицирован или удален)
    LEADERBOARD_NOT_VALID_AUTHOR = 24;

    // Коды ошибок при регистрации
    REG_INVALID_PHONE_NUMBER = 100; // номер телефона задан неверно
    REG_INVALID_PHONE_TOKEN = 101; // не верный токен подтверждения телефона (возможно устарел)
    REG_INVALID_PHONE_CODE = 102; // не верный код подтверждения

    // Коды ошибок внутри SKLLZZ CLUB
    CLUB_NOT_FOUND = 500; // Клуб не найден
    CLUB_CLIENT_NOT_FOUND = 501; // Клиент с указанными номером телефона не найден в клубе
    CLUB_INSUFFICIENT_FUNDS = 502; // не достаточно средств на лицевых счетах клиента
    CLUB_REQUIRE_PAYED_SERVICE = 503; // требуется покупка необходимой услуги
    CLUB_INSUFFICIENT_SLOTS = 504; // нет свободных мест
    CLUB_CLASS_IS_OVER = 505; // занятие уже проведено
    CLUB_UNABLE_TO_PROCESS_REQUEST = 506; // клубное по не может выполнить запрашиваемую операцию (обратитесь на ресепшн)
    CLUB_UNABLE_TO_FREEZE = 507; // клубное по не может выполнить заморозку контракта (обратитесь на ресепшн)
    CLUB_UNABLE_TO_UNFREEZE = 508; // клубное по не может выполнить разморозку контракта (обратитесь на ресепшн)
    CLUB_CLIENT_LIMIT = 509; // превышено допустимое количество клиентов клуба
    CLUB_UNDER_CONSTRUCTION = 510; // функция в разработке
    CLUB_APPROVE_PENDING = 511; // подключение к клубу на рассмотрении
    CLUB_UNABLE_TO_LEAVE_DUE_TO_INCOMPLETE_CHALLENGE = 512; // в клубе есть не завершенные соревнования
    REQUIRE_JOIN_CLUB = 513; // требуется подключение к клубу
    APP_UPGRADE_REQUIRED = 1000; // требуется обновление приложения

  }
  Code code = 1;
  bool offer_pro = 2;
  string details = 3; // дополнительное текстовое сообщение (не требующее локализации)
  PaymentOffer offer_payment = 4; // предложения на оплату для CLUB_INSUFFICIENT_FUNDS
  repeated ServiceOffer offer_service = 5; // предложения на покупку услуги для CLUB_REQUIRE_PAYED_SERVICE
  int64 int_arg1 = 6; // произвольный числовой параметр ошибки
  int64 int_arg2 = 7; // произвольный числовой параметр ошибки
  string str_arg1 = 16; // произвольный строковый аргумент
}

message PaymentOffer {
  string id = 1; // uid идентификатор платежа
  string url = 2; // url для перехода
}

message ServiceOffer {
  string id = 1; // uid идентификатор услуги
  string title = 2; // название услуги
  string description = 3; // описание услуги
  string photo_url = 4; // картинка для услуги
  float price = 5; // стоимость услуги
}
